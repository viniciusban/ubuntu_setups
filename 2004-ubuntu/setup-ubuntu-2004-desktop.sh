#!/usr/bin/env bash

# SETUP UBUNTU 18.04
# ==================

read -p "Have you read the README.md file from this repo? [yes/no] " yn

[ "$yn" != "yes" ] && return;



# Basic stuff
# -----------

sudo apt update
sudo apt upgrade -y
sudo apt install -fy

# general command line
sudo apt install -y curl tree vim wget xsel

sudo apt install -y filezilla tilix

sudo apt install -y ffmpeg libavcodec-extra
sudo apt install -y ubuntu-restricted-extras

sudo apt install -y build-essential make libssl-dev zlib1g-dev libbz2-dev libreadline-dev libsqlite3-dev



# Create basic directories
# ------------------------
mkdir --parent ~/.local/share/fonts ~/.local/bin
mkdir --parent ~/tmp


# Private key stuff
# -----------------

# Disable Gnome Keyring window asking to type passphrase (a.k.a "make ssh ask for passphrase in terminal").
# The only way to disable it (as far as I know) is completely disabling Gnome Keyring.
[ ! -e /usr/bin/gnome-keyring-daemon.disabled ] && sudo mv /usr/bin/gnome-keyring-daemon /usr/bin/gnome-keyring-daemon.disabled
pkill -f gnome-keyring-daemon
sudo apt-mark hold gnome-keyring

# Set private key permissions
#chmod go-rw ~/.ssh/id_rsa
#chmod go-w ~/.ssh/id_rsa.pub


# Newer git
# ---------

sudo apt remove -y git
sudo add-apt-repository -y ppa:git-core/ppa
sudo apt update -y
sudo apt install -y git


# Newer tmux
# ----------

# See dependencies and build commands in https://github.com/tmux/tmux/blob/master/.travis.yml
#T=$(mktemp --tmpdir -d tmux-XXXXX.d)
#git clone https://github.com/tmux/tmux.git $T	
#cd $T
#git checkout 2.6
#sudo apt install -y debhelper autotools-dev dh-autoreconf file libncurses5-dev libevent-dev pkg-config libutempter-dev build-essential
#(CFLAGS= ./autogen.sh) && ./configure --enable-debug && make
#mv $T/tmux ~/.local/bin
#cd
#rm -rf $T


# virtualbox and vagrant
# ----------------------

sudo apt install -y virtualbox
sudo usermod -a -G vboxusers $USER

#curl -fsSL https://apt.releases.hashicorp.com/gpg | sudo apt-key add -
#sudo apt-add-repository "deb [arch=amd64] https://apt.releases.hashicorp.com $(lsb_release -cs) main"
#sudo apt-get update && sudo apt-get install vagrant


# IBM Plex font
# -------------

T=$(mktemp --tmpdir -d ibm-plex-XXXXX.d)
cd $T
wget https://github.com/IBM/plex/archive/master.zip
unzip master
mkdir --parent ~/.local/share/fonts/ibm-plex-mono
cp plex-master/IBM-Plex-Mono/fonts/complete/ttf/* ~/.local/share/fonts/ibm-plex-mono/
cd
rm -rf $T


# Tweak locale
# ------------

[ ! -e ~/.pam_environment.original ] && cp ~/.pam_environment ~/.pam_environment.original && chmod -w ~/.pam_environment.original
cat > ~/.pam_environment <<_EOD_
LANGUAGE=en_GB:en
LANG=en_GB.UTF-8
LC_NUMERIC=pt_BR.UTF-8
LC_TIME=en_GB.UTF-8
LC_MONETARY=pt_BR.UTF-8
LC_PAPER=pt_BR.UTF-8
LC_NAME=pt_BR.UTF-8
LC_ADDRESS=pt_BR.UTF-8
LC_TELEPHONE=pt_BR.UTF-8
LC_MEASUREMENT=pt_BR.UTF-8
LC_IDENTIFICATION=en_GB.UTF-8
PAPERSIZE=a4
_EOD_


function install_python_stuff () {
    # pyenv
    # -----

    git clone https://github.com/pyenv/pyenv.git ~/.local/bin/pyenv

    sudo apt install -y make build-essential libssl-dev zlib1g-dev libbz2-dev libreadline-dev libsqlite3-dev wget curl llvm libncurses5-dev libncursesw5-dev xz-utils tk-dev
    sudo apt install -y liblzma-dev libgdbm-dev

    export PYENV_ROOT="$HOME/.local/bin/pyenv"
    export PATH="$PYENV_ROOT/bin:$PATH"
    eval "$(pyenv init -)"

    pyenv install $PY2_VERSION
    pyenv install $PY3_VERSION
    pyenv global $PY3_VERSION $PY2_VERSION

    # pyenv virtualenv
    git clone https://github.com/pyenv/pyenv-virtualenv.git $(pyenv root)/plugins/pyenv-virtualenv

    # tools with Python 3
    pyenv virtualenv $PY3_VERSION tools
    pyenv activate tools
    pip install youtube-dl
    pyenv deactivate

    # jupyter global with Python 3
    pyenv virtualenv $PY3_VERSION jupyter
    pyenv activate jupyter
    pip install jupyter
    python -m ipykernel install --user

    # Make jupyter recognize my active virtualenv
    # Reference to jupyter and virtualenv config:
    # - https://medium.com/welcome-to-the-django/guia-definitivo-para-organizar-meu-ambiente-python-a16e2479b753
    mkdir --parent ~/.ipython/profile_default/startup
    curl -L https://gist.githubusercontent.com/viniciusban/bdf70f1ce36c02673731e7b0eeb05615/raw/56e646054e62efb079af3666102c381fa571065c/00-detect-virtualenv-sitepackages.py > ~/.ipython/profile_default/startup/00-venv-sitepackages.py
    pyenv deactivate

    # Make pyenv permanent
    cat >> ~/.bash_aliases <<_EOD_
export PYENV_ROOT=\$HOME/.local/bin/pyenv
export PATH=\$PYENV_ROOT/bin:\$PATH
eval "\$(pyenv init -)"
pyenv global $PY3_VERSION $PY2_VERSION tools jupyter
_EOD_
}

PY3_VERSION=3.6.5
PY2_VERSION=2.7.14
#install_python_stuff
