Setup Ubuntu 18.04
==================


Just after installing Ubuntu 18.04, proceed to the following settings:

1. Configure main language to United Kingdom:

   - Drag "United Kingdom" to the top of list;
   - Click "Apply system-wide";
   - Close;
   - reboot.

2. Then open your terminal and type:

        $ sudo apt update
        $ sudo apt -y upgrade
        $ sudo reboot

2. Copy your private and public ssh keys to `~/.ssh/` directory:

        $ mkdir ~/.ssh
        $ cd ~/.ssh

   Now, you copy your key contents into `id_rsa` and `id_rsa.pub` files.

5. Install git and clone this very repository:

        $ sudo apt -y install git
        $ mkdir ~/src
        $ git clone <this_repo_url> ~/src/ubuntu_setups

6. Run setup process:

        $ cd ~/src/ubuntu_setups
        $ ./setup-ubuntu-1804.sh

