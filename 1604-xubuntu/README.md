Setup Xubuntu 16.04
===================

I use [pyenv](https://github.com/pyenv/pyenv) because it doesn't mess with my
O.S and allows me to have any Python version.

Install Xubuntu disabling internet access to run faster and reboot it.

Then open your terminal and type:

    $ sudo apt update
    $ sudo apt -y upgrade
    $ sudo reboot


Copy your ssh keys in `~/.ssh/`.

Proceed to install stuff:

    $ ./setup-xubuntu-16.04-desktop.sh
