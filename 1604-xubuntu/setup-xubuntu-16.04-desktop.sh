#!/usr/bin/env bash

# SETUP XUBUNTU 16.04
# ===================


# Basic stuff
# -----------

sudo apt update
sudo apt upgrade -y 
sudo apt install -fy 

sudo apt install -y tree curl xsel terminator
sudo apt install -y vim-gtk
sudo apt install -y gnome-system-monitor

sudo apt install -y build-essential make libssl-dev zlib1g-dev libbz2-dev libreadline-dev libsqlite3-dev

sudo apt install -y virtualbox
sudo usermod -a -G vboxusers $USER
wget -O /tmp/vagrant.deb https://releases.hashicorp.com/vagrant/1.9.8/vagrant_1.9.8_x86_64.deb
sudo dpkg -i /tmp/vagrant.deb && rm /tmp/vagrant.deb

sudo apt install -y filezilla chromium-browser


# Newer packages
# --------------

mkdir --parent ~/.local/bin

# Newer git
sudo add-apt-repository ppa:git-core/ppa
sudo apt update
sudo apt install -y git

# Newer tmux
# See dependencies and build commands in https://github.com/tmux/tmux/blob/master/.travis.yml
cd /tmp
git clone https://github.com/tmux/tmux.git
cd tmux
git checkout 2.3
sudo apt install -y debhelper autotools-dev dh-autoreconf file libncurses5-dev libevent-dev pkg-config libutempter-dev build-essential
(CFLAGS= ./autogen.sh) && ./configure --enable-debug && make
mv /tmp/tmux/tmux ~/.local/bin



# pyenv
# -----
#
# Reference to jupyter and virtualenv config:
#
# - https://medium.com/welcome-to-the-django/guia-definitivo-para-organizar-meu-ambiente-python-a16e2479b753


git clone https://github.com/pyenv/pyenv.git ~/.pyenv

sudo apt install -y make build-essential libssl-dev zlib1g-dev libbz2-dev libreadline-dev libsqlite3-dev wget curl llvm libncurses5-dev libncursesw5-dev xz-utils tk-dev

export PYENV_ROOT="$HOME/.pyenv"
export PATH="$PYENV_ROOT/bin:$PATH"
eval "$(pyenv init -)"

pyenv install 2.7.13
pyenv install 3.6.1
pyenv global 3.6.1 2.7.13

echo 'export PYENV_ROOT="$HOME/.pyenv"' >> ~/.bash_aliases
echo 'export PATH="$PYENV_ROOT/bin:$PATH"' >> ~/.bash_aliases
echo 'eval "$(pyenv init -)"' >> ~/.bash_aliases
echo 'pyenv global 3.6.1 2.7.13' >> ~/.bash_aliases

# pyenv virtualenv
git clone https://github.com/pyenv/pyenv-virtualenv.git $(pyenv root)/plugins/pyenv-virtualenv
exec "$SHELL"

# jupyter with Python 3
pyenv virtualenv 3.6.1 jupyter3
pyenv activate jupyter3
pip install jupyter
python -m ipykernel install --user

# Make jupyter recognize my active virtualenv
curl -L https://gist.githubusercontent.com/henriquebastos/270cff100cb303f3d74370489022446b/raw/56e646054e62efb079af3666102c381fa571065c/00-detect-virtualenv-sitepackages.py > ~/.ipython/profile_default/startup/00-venv-sitepackages.py
pyenv deactivate

# tools with Python 3
pyenv virtualenv 3.6.1 tools3
pyenv activate tools3
pip install youtube-dl
pyenv deactivate


# Disable gnome-keyring
# ---------------------
#
# Make ssh ask for private key passphrase in terminal (disable window to type
# passphrase).

sudo mv /usr/bin/gnome-keyring-daemon /usr/bin/gnome-keyring-daemon.disabled
pkill -f gnome-keyring-daemon
# don't update gnome-keyring
apt-mark hold gnome-keyring


# Set private key permissions
# ---------------------------

chmod g-rw ~/.ssh/id_rsa
chmod o-r ~/.ssh/id_rsa


# Source Code Pro font
# --------------------

cd /tmp
mkdir sourcecodepro
wget -O source-code-pro.zip https://www.fontsquirrel.com/fonts/download/source-code-pro
unzip -d sourcecodepro/ source-code-pro.zip
mkdir ~/.fonts
mv sourcecodepro ~/.fonts


# avconv to convert multimedia files
# ----------------------------------

sudo apt install -y libav-tools libavcodec-extra
sudo apt install -y ubuntu-restricted-extras


# Create useful directories
# -------------------------

mkdir ~/projects
mkdir ~/virtualenvs
mkdir ~/tmp
