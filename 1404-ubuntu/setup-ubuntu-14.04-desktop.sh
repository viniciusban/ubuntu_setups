#!/usr/bin/env bash

## ---------------------------------------------------------------------------
## Script to run a guided setup to your Ubuntu 14.04 Trusty Tahr.
## by @viniciusban
##
##
## Notes:
##   - You'll need an active internet connection.
##   - You can test and run this script in a virtual machine, before doing it
##     in your real environment.
##
##
## IMPORTANT:
## 1. I do not guarantee this will work as you want.
## 2. You can lose data. So, make a backup before running it for real.
##
## ---------------------------------------------------------------------------


function _main () {
  cd $HOME
  USER=$(basename $HOME)

  _initial_delay

  _am_i_sudo
  if [ $? -eq 0 ]; then
      _update_packages
      _sudo_menu
      _cleanup
      _show_ecryptfs_instructions
  else
      _install_ssh_keys
      _configure_gnome_keyring_to_cache_passphrase_based_on_idle_time
      _non_sudo_menu
  fi
}


function _am_i_sudo () {
  # if [ "$(whoami)" != "root" ]; then
  #   echo "You must be in root mode."
  #   echo "$ sudo $0"
  #   exit 1
  # fi

  if [ "$(whoami)" = "root" ]; then
      return 0
  else
      return 1
  fi
}


function _initial_delay () {
  echo "

IF YOU DON'T WANT TO RUN THIS SCRIPT, PRESS CTRL+C TO STOP.

"
  sleep 3

  echo "The installation will begin in 15 seconds..."

  for n in {14..0}; do
    sleep 1
    if [ $n -eq 10 ]; then
      echo "$n seconds and we'll update your system with basic packages..."
    elif [ $n -eq 5 ]; then
      echo "$n seconds and you still can cancel with CTRL+C..."
    else
      echo "$n seconds..."
    fi
  done

  sleep 3
  echo "IT'S SHOW TIME!"
  sleep 3
}


function _update_packages () {
  apt-get -y update
  apt-get -y upgrade

  # fix broken dependencies
  apt-get -fy install
}



function _sudo_menu () {
  read -p "Do you want to install basic packages? (y/n)" yn
  if [ "$yn" = "y" ] || [ "$yn" = "Y" ]; then
    echo "_install_basic_packages"
    _install_basic_packages
  fi

  read -p "Do you want to install and configure TLP to improve battery life? (y/n)" yn
  if [ "$yn" = "y" ] || [ "$yn" = "Y" ]; then
    echo "_install_and_config_tlp"
    _install_and_config_tlp
  fi

  read -p "Do you want to disable Bluetooth? (y/n)" yn
  if [ "$yn" = "y" ] || [ "$yn" = "Y" ]; then
    echo "_disable_bluetooth"
    _disable_bluetooth
  fi

  read -p "Do you want to install useful command line packages? (y/n)" yn
  if [ "$yn" = "y" ] || [ "$yn" = "Y" ]; then
    echo "_install_useful_cli_packages"
    _install_useful_cli_packages
  fi

  read -p "Do you want to install additional fonts? (y/n)" yn
  if [ "$yn" = "y" ] || [ "$yn" = "Y" ]; then
    echo "_install_fonts"
    _install_fonts
  fi

  read -p "Do you want to install packages to use at work? (y/n)" yn
  if [ "$yn" = "y" ] || [ "$yn" = "Y" ]; then
    echo "_install_work_packages"
    _install_work_packages
  fi

  read -p "Do you want to install multimedia packages? (y/n)" yn
  if [ "$yn" = "y" ] || [ "$yn" = "Y" ]; then
    echo "_install_multimedia_packages"
    _install_multimedia_packages
  fi

  read -p "Do you want to install Ubuntu and Unity tweak packages? (y/n)" yn
  if [ "$yn" = "y" ] || [ "$yn" = "Y" ]; then
    echo "_install_tweak_packages"
    _install_tweak_packages
  fi

  read -p "Do you want to install LAMP server? (y/n)" yn
  if [ "$yn" = "y" ] || [ "$yn" = "Y" ]; then
    echo "_install_lamp_server"
    _install_lamp_server
  fi

  read -p "Do you want to remove Thunderbird? (y/n)" yn
  if [ "$yn" = "y" ] || [ "$yn" = "Y" ]; then
    echo "_remove_thunderbird"
    _remove_thunderbird
  fi

  read -p "Do you want to completely disable zeitgeist? (y/n)" yn
  if [ "$yn" = "y" ] || [ "$yn" = "Y" ]; then
    echo "_disable_zeitgeist"
    _disable_zeitgeist
  fi

  read -p "Do you want to make basic tweaks? (y/n)" yn
  if [ "$yn" = "y" ] || [ "$yn" = "Y" ]; then
    echo "_make_basic_tweaks"
    _make_basic_tweaks
  fi
}


# --------------------------------------------------
# FUNCTIONS TO WORK WITH SUDO
# --------------------------------------------------


function _install_basic_packages () {
  apt-get install -y curl
  apt-get install -y tree
  apt-get install -y git
  apt-get install -y openssh-server # allows connection from a neighbour computer
  apt-get install -y python-dev python3-dev
  apt-get install -y python-pip python3-pip

  apt-get install -y vim-gtk
  apt-get install -y tmux
  apt-get install -y make build-essential libssl-dev zlib1g-dev libbz2-dev libreadline-dev libsqlite3-dev
}


function _install_and_config_tlp () {
  add-apt-repository -y ppa:linrunner/tlp
  apt-get -y update
  apt-get install -y tlp tlp-rdw

  cp --backup=numbered /etc/default/tlp /etc/default/tlp.original.$USER

  # Adjust cpu frequency governors
  sed -i 's/#\(CPU_SCALING_GOVERNOR_ON_AC\)=.*/\1=ondemand/' /etc/default/tlp
  sed -i 's/#\(CPU_SCALING_GOVERNOR_ON_BAT\)=.*/\1=powersave/' /etc/default/tlp

  # Don't powersave wifi
  sed -i 's/\(WIFI_PWR_ON_BAT\)=.*/\1=1/' /etc/default/tlp

  # Radeon profile low
  sed -i 's/\(RADEON_POWER_PROFILE_ON_AC\)=.*/\1=low/' /etc/default/tlp

  tlp start
}


function _disable_bluetooth () {
  # prevent from starting
  mv /etc/init/bluetooth.conf /etc/init/bluetooth.conf.disabled.$USER

  # Disable bluetooth in tlp (if tlp is installed)
  if [ -e /etc/default/tlp ]; then
    cp --backup=numbered /etc/default/tlp /etc/default/tlp.original.$USER
  fi

  sed -i 's/#\(DEVICES_TO_DISABLE_ON_STARTUP\)=.*/\1="bluetooth"/' /etc/default/tlp
}


function _cleanup () {
  chown -R ${USER}:${USER} ~/
#   apt-get upgrade
  apt-get autoremove
}


function _show_ecryptfs_instructions () {
  mount | grep $USER | grep ecryptfs > /dev/null 2>&1
  if [ $? -eq 0 ]; then
    echo "
## ----------------------------------------------------------------------
YOU HAVE ENCRYPTED YOUR HOME DIR.
USE THE COMMAND BELOW TO RECALL THE PASSPHRASE:
\$ ecryptfs-unwrap-passphrase

IT WILL ASK FOR YOUR PASSWORD TO SHOW YOU THE CODE.
## ----------------------------------------------------------------------

"
  fi
}


function _install_useful_cli_packages () {
  apt-get install -y xsel  # allow copy to clipboard from command line.
  apt-get install -y wmctrl # allow shell script to maximize a window
  apt-get install -y youtube-dl
}


function _install_work_packages () {
  apt-get install -y terminator
  apt-get install -y dreampie
  apt-get install -y chromium-browser
  apt-get install -y filezilla
  apt-get install -y gparted
  apt-get install -y hamster-indicator
  apt-get install -y indicator-cpufreq
  apt-get install -y pinta
  apt-get install -y meld # git merge.tool
  apt-get install -y sqlite3
  apt-get install -y sqliteman
  pip install pep8
  pip install autopep8

  # Virtualbox
#   echo "deb http://dowload.virtualbox.org/virtualbox/debian $(lsb_release -sc) contrib" | tee -a /etc/apt/sources.list
#   wget -q http://download.virtualbox.org/virtualbox/debian/oracle_vbox.asc -O- | apt-key add -
#   apt-get -y update
  apt-get install -y virtualbox
  usermod -a -G vboxusers

  # ReText (markdown offline editor with live preview)
  apt-add-repository -y ppa:mitya57/ppa
  apt-get -y update
  apt-get install -y retext

  # indicator-keylock to work in machines without capslock/numlock lights.
  apt-add-repository -y ppa:tsbarnes/indicator-keylock
  apt-get -y update
  apt-get install -y indicator-keylock

  # pomodoro-indicator
  add-apt-repository -y ppa:atareao/atareao
  apt-get -y update
  apt-get install -y pomodoro-indicator

  # Vagrant (download and install)
  wget -O /tmp/vagrant.deb https://dl.bintray.com/mitchellh/vagrant/vagrant_1.5.3_x86_64.deb
  dpkg -i /tmp/vagrant.deb && rm /tmp/vagrant.deb

  # Skype
  echo 'deb http://archive.canonical.com/ubuntu/ trusty partner' >> /etc/apt/sources.list.d/canonical_partner.list
  apt-get -y update
  apt-get install -y skype
}


function _install_multimedia_packages () {
  apt-get install -y libav-tools # avconv (to replace ffmpeg)
  apt-get install -y libavcodec-extra
  apt-get install -y ubuntu-restricted-extras

  # jre and browser plugin
  apt-get install -y icedtea-7-plugin openjdk-7-jre
  apt-get install -y libdvdread4

  # enable playing encrypted DVD
  /usr/share/doc/libdvdread4/install-css.sh

  # fix firefox inability to play H.264 video with html5 player
  add-apt-repository -y ppa:mc3man/trusty-media
  apt-get update
  apt-get install -y gstreamer0.10-ffmpeg

  # use adobe flash with chromium
  apt-get install -y pepperflashplugin-nonfree
  update-pepperflashplugin-nonfree --install
}


function _install_tweak_packages () {
  # ccsm
  apt-get install -y compizconfig-settings-manager

  # unity tweak tool
  apt-get install -y unity-tweak-tool
}


function _remove_thunderbird () {
  apt-get purge -y thunderbird
}


function _install_fonts () {
  # monaco
  mkdir --parent ~/.fonts
  cd ~/.fonts
  wget -c https://github.com/cstrap/monaco-font/raw/master/Monaco_Linux.ttf
  cd -

  # inconsolata
  apt-get install -y fonts-inconsolata
  fc-cache -f -v
}


function _disable_zeitgeist () {
  # from running
  mv /usr/bin/zeitgeist-daemon /usr/bin/zeitgeist-daemon.disabled
  mv /usr/bin/zeitgeist-datahub /usr/bin/zeitgeist-datahub.disabled

  # from updating
  apt-mark hold zeitgeist-core
  apt-mark hold zeitgeist-datahub
}


function _make_basic_tweaks () {
  # disable some smart lens to accelerate search in dash home
  gsettings set com.canonical.Unity.Lenses disabled-scopes "['more_suggestions-amazon.scope', 'more_suggestions-u1ms.scope', 'more_suggestions-populartracks.scope', 'music-musicstore.scope', 'more_suggestions-ebay.scope', 'more_suggestions-ubuntushop.scope', 'more_suggestions-skimlinks.scope', 'files-local.scope']"

  # restrict Unity lens search
  gsettings set com.canonical.Unity.Lenses always-search "['applications.scope', 'files.scope']"
  gsettings set com.canonical.Unity.ApplicationsLens display-available-apps false

  # disable file usage remembering
  gsettings set org.gnome.desktop.privacy remember-recent-files false

  # hide keyboard from Unity menu bar
  gsettings set com.canonical.indicator.keyboard visible false

  # touchpad with natural scroll
  gsettings set org.gnome.settings-daemon.peripherals.touchpad natural-scroll true
  gsettings set org.gnome.settings-daemon.peripherals.touchpad motion-acceleration 10

  # nautilus preference
  gsettings set org.gnome.nautilus.preferences sort-directories-first true
}


function _install_lamp_server () {
  echo 'mysql-server mysql-server/root_password password root' | debconf-set-selections
  echo 'mysql-server mysql-server/root_password_again password root' | debconf-set-selections
  apt-get install --force-yes -y tasksel
  apt-get update # needed to update tasksel apps.
  tasksel install lamp-server
}


# --------------------------------------------------
# FUNCTIONS TO WORK WITHOUT SUDO
# --------------------------------------------------

function _non_sudo_menu () {
  read -p "Do you want to install Python virtualenv with pyenv? (y/n)" yn
  if [ "$yn" = "y" ] || [ "$yn" = "Y" ]; then
    echo "_install_and_config_python_virtualenvs"
    _install_and_config_python_virtualenvs
  fi

  read -p "Do you want to install dotfiles? (y/n)" yn
  if [ "$yn" = "y" ] || [ "$yn" = "Y" ]; then
    echo "_install_dotfiles"
    _install_dotfiles
  fi

  read -p "Do you want to install vimfiles? (y/n)" yn
  if [ "$yn" = "y" ] || [ "$yn" = "Y" ]; then
    echo "_install_vimfiles"
    _install_vimfiles
  fi

  read -p "Do you want to install bin scripts? (y/n)" yn
  if [ "$yn" = "y" ] || [ "$yn" = "Y" ]; then
    echo "_install_binscripts"
    _install_binscripts

    read -p "Do you want to swap CTRL and CAPS LOCK keys at startup? (y/n)" yn
    if [ "$yn" = "y" ] || [ "$yn" = "Y" ]; then
      echo "_swap_ctrl_and_caps_lock"
      _swap_ctrl_and_caps_lock
    fi
  fi

}


function _install_and_config_python_virtualenvs () {
  # pip install ipython
  # pip install pytest

  # install pyenv
  curl https://raw.github.com/yyuu/pyenv-installer/master/bin/pyenv-installer | bash

  pyenv install 2.7.6
  pyenv install 3.4.0

  pyenv global 2.7.6
  pip install virtualenvwrapper
}


function _install_ssh_keys () {
  DEFAULT_SSH_REPO_URL="https://gitlab.com/viniciusban/ssh_keys.git"

  echo "To continue from this step, you must install your private .ssh keys."
  read -p "Fill in your .ssh keys repo URL (default is $DEFAULT_SSH_REPO_URL): " SSH_REPO_URL

  mkdir --parent ~/.ssh
  git clone ${SSH_REPO_URL:-$DEFAULT_SSH_REPO_URL} ~/.ssh
}

function _configure_gnome_keyring_to_cache_passphrase_based_on_idle_time () {
    # Original entries:
    # org.gnome.crypto.cache gpg-cache-method 'session'
    # org.gnome.crypto.cache gpg-cache-ttl 300

    # cache password for 3 minutes (180 seconds)
    gsettings set org.gnome.crypto.cache gpg-cache-method 'idle'
    gsettings set org.gnome.crypto.cache gpg-cache-ttl 180
}


function _install_dotfiles () {
  DEFAULT_DOTFILES_REPO_URL="git@gitlab.com:viniciusban/dotfiles.git"

  read -p "Fill in your dotfiles repo URL (default is $DEFAULT_DOTFILES_REPO_URL): " DOTFILES_REPO_URL

  cd ~
  git clone ${DOTFILES_REPO_URL:-$DEFAULT_DOTFILES_REPO_URL} ~/dotfiles
  ~/dotfiles/create_dotfiles_symlinks.sh
}


function _install_vimfiles () {
  DEFAULT_VIMFILES_REPO_URL="git@gitlab.com:viniciusban/vimfiles.git"

  read -p "Fill in your vimfiles repo URL (default is $DEFAULT_VIMFILES_REPO_URL): " VIMFILES_REPO_URL

  cd ~
  git clone ${VIMFILES_REPO_URL:-$DEFAULT_VIMFILES_REPO_URL} ~/.vim
}


function _install_binscripts () {
  DEFAULT_BINSCRIPTS_REPO_URL="git@gitlab.com:viniciusban/binscripts.git"

  read -p "Fill in your binscripts repo URL (default is $DEFAULT_BINSCRIPTS_REPO_URL): " BINSCRIPTS_REPO_URL

  cd ~
  git clone ${BINSCRIPTS_REPO_URL:-$DEFAULT_BINSCRIPTS_REPO_URL} ~/bin
}


function _swap_ctrl_and_caps_lock () {
  local DIR="/home/viniciusban/.config/autostart"
  mkdir --parent $DIR
  echo "[Desktop Entry]
Type=Application
Exec=/home/viniciusban/bin/swap-ctrl-and-caps-lock.sh
Hidden=false
NoDisplay=false
X-GNOME-Autostart-enabled=true
Name[en_US]=Swap CTRL and CAPS LOCK keys
Name=Swap CTRL and CAPS LOCK keys
Comment[en_US]=Less hand moving when using VIM keybindings.
Comment=Less hand movements to VIM keybindings
" | tee ${DIR}/swap-ctrl-and-caps-lock.sh.desktop > /dev/null
}


_main
